package com.example.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



//menggunakan metode implements di public class agar tidak perlu mengimplements button satu persatu
public class halaman1 extends AppCompatActivity implements View.OnClickListener {

//mendefinisikan button dan Textview pada layout
    Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt0;
    Button bttambah, btkali, btkurang, btbagi, bthasil, btclear, btkoma;
    TextView angka;

//mendefinisikan float bilangan 1 dan bilangan 2
    float Bil1, Bil2;
//mendefinisikan boolean yang digunakan untuk operasi
    boolean Tambah, Kurang, Kali, Bagi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//men-set konten view untuk dihubungkan dengan layout
        setContentView(R.layout.halaman1);

//mendeklarasikan variabel untuk dihubungan dengan id pada layout
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        bt3 = findViewById(R.id.bt3);
        bt4 = findViewById(R.id.bt4);
        bt5 = findViewById(R.id.bt5);
        bt6 = findViewById(R.id.bt6);
        bt7 = findViewById(R.id.bt7);
        bt8 = findViewById(R.id.bt8);
        bt9 = findViewById(R.id.bt9);
        bt0 = findViewById(R.id.bt0);
        bttambah = findViewById(R.id.bttambah);
        btkali = findViewById(R.id.btkali);
        btkurang = findViewById(R.id.btkurang);
        btbagi = findViewById(R.id.btbagi);
        bthasil = findViewById(R.id.bthasil);
        btclear = findViewById(R.id.btclear);
        btkoma = findViewById(R.id.btkoma);
        angka = findViewById(R.id.angka);

//Menghubungkan button dengan OnCLickListener
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);
        bt5.setOnClickListener(this);
        bt6.setOnClickListener(this);
        bt7.setOnClickListener(this);
        bt8.setOnClickListener(this);
        bt9.setOnClickListener(this);
        bt0.setOnClickListener(this);
        btclear.setOnClickListener(this);
        bttambah.setOnClickListener(this);
        btkurang.setOnClickListener(this);
        btkali.setOnClickListener(this);
        btbagi.setOnClickListener(this);
        bthasil.setOnClickListener(this);
        btkoma.setOnClickListener(this);
    }

    @Override
//Mensetting button untuk menampilkan angka 0-9 pada textview
    public void onClick(View view) {
        if (view.getId() == R.id.bt1) {
            angka.setText(angka.getText() + "1");
        } else if (view.getId() == R.id.bt2) {
            angka.setText(angka.getText() + "2");
        } else if (view.getId() == R.id.bt3) {
            angka.setText(angka.getText() + "3");
        } else if (view.getId() == R.id.bt4) {
            angka.setText(angka.getText() + "4");
        } else if (view.getId() == R.id.bt5) {
            angka.setText(angka.getText() + "5");
        } else if (view.getId() == R.id.bt6) {
            angka.setText(angka.getText() + "6");
        } else if (view.getId() == R.id.bt7) {
            angka.setText(angka.getText() + "7");
        } else if (view.getId() == R.id.bt8) {
            angka.setText(angka.getText() + "8");
        } else if (view.getId() == R.id.bt9) {
            angka.setText(angka.getText() + "9");
        } else if (view.getId() == R.id.bt0) {
            angka.setText(angka.getText() + "0");
        }

//Mensetting button untuk menampilkan koma dan menghapus angka pada textview
        if (view.getId() == R.id.btclear) {
            angka.setText("");
        } else if (view.getId() == R.id.btkoma) {
            angka.setText(angka.getText() + ".");
        }

//Mensetting button operasi aritmatika, jika angka = null, textview tidak menampilkan apa2
//Jika angka = not null, maka dua bilangan akan dioperasikan
//Parsefloat digunakan untuk mengubah bilangan non-float menjadi float
//boolean bernilai true agar dapat dioperasikan pada button hasil
          if (view.getId() == R.id.bttambah) {
            if (angka == null) {
                angka.setText("");
            } else {
                Bil1 = Float.parseFloat(angka.getText() + "");
                Tambah = true;
                angka.setText(null);
            }
        } else if (view.getId() == R.id.btkurang) {
              if (angka == null) {
                  angka.setText("");
              } else {
                  Bil1 = Float.parseFloat(angka.getText() + "");
                  Kurang = true;
                  angka.setText(null);
              }
        } else if (view.getId() == R.id.btbagi) {
              if (angka == null) {
                  angka.setText("");
              } else {
                  Bil1 = Float.parseFloat(angka.getText() + "");
                  Bagi = true;
                  angka.setText(null);
              }
        } else if (view.getId() == R.id.btkali) {
              if (angka == null) {
                  angka.setText("");
              } else {
                  Bil1 = Float.parseFloat(angka.getText() + "");
                  Kali = true;
                  angka.setText(null);
              }
        }

//Mensetting button hasil operasi aritmatika
//Boolean bernilai true jika pengguna mengklik button operasi, dan akan menjumlahkan dua bilangan
//string operasi digunakan untuk menampilkan operasi di halaman kedua / acitivy selanjutnya
          if (view.getId() == R.id.bthasil) {
              Bil2 = Float.parseFloat(angka.getText() + "");
              String operasi = "";
              if (Tambah == true) {
                  angka.setText(Bil1 + Bil2 + "");
                  operasi = Bil1 + " + " + Bil2;
                  Tambah = false;
              }
              if (Kurang == true) {
                  angka.setText(Bil1 - Bil2 + "");
                  operasi = Bil1 + " - " + Bil2;
                  Kurang = false;
              }
              if (Bagi == true) {
                  angka.setText(Bil1 / Bil2 + "");
                  operasi = Bil1 + " / " + Bil2;
                  Bagi = false;
              }
              if (Kali == true) {
                  angka.setText(Bil1 * Bil2 + "");
                  operasi = Bil1 + " * " + Bil2;
                  Kali = false;
              }

//menggunakan intent untuk menampilkan operasi dan hasil operasi di halaman kedua / activity selanjutnya
//meletakkan string "Hasil" yang berisi angka dan string "Operasi" yang berisi operasi ke intent
              Intent intent =new Intent(this, halaman2.class);
              intent.putExtra("Hasil", angka.getText().toString());
              intent.putExtra("Operasi", operasi);
              startActivity(intent);
          }
    };
}



